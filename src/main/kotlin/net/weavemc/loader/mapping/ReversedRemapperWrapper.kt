package net.weavemc.loader.mapping

import org.objectweb.asm.commons.Remapper

open class ReversedRemapperWrapper(private val mapper: IMapper) : Remapper() {
    override fun map(internalName: String): String? {
        val name = internalName.replaceFirst("org/objectweb", "net/weavemc")
        return mapper.reverseMapClass(name) ?: name
    }

    override fun mapMethodName(owner: String, name: String, descriptor: String): String? = mapper.reverseMapMethod(owner, name, descriptor)?.name ?: name

    override fun mapFieldName(owner: String, name: String, descriptor: String): String? = mapper.reverseMapField(owner, name)?.name ?: name

    fun getMapperName(): String = mapper::class.simpleName!!
}
